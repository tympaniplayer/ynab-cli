use budget::YnabClient;
use std::io;
use structopt::StructOpt;
use ynab_cli::budget;

#[derive(Debug, StructOpt)]
#[structopt(
    name = "ynab-cli",
    about = "A way to categorize and approve transactions in ynab."
)]
struct Opt {
    #[structopt(short, long)]
    apikey: String,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opt = Opt::from_args();

    println!("{:#?}", opt);

    let client: YnabClient = YnabClient::new(opt.apikey);

    let budgets = budget::list_budgets(&client).await?;

    let selected_budget = budget::choose_budget(&budgets, &mut io::stdout())?;

    let transactions = budget::get_uncleared_transactions(&client, &selected_budget).await?;

    let mut selected_transaction = budget::choose_transaction(&transactions, &mut io::stdout())?;

    let selected_category_group =
        budget::choose_category_group(&client, &selected_budget, &mut io::stdout()).await?;

    let selected_category =
        budget::choose_category_from_group(&selected_category_group, &mut io::stdout())?;

    budget::update_category_for_transaction(
        &client,
        &selected_category,
        &selected_budget,
        &mut selected_transaction,
    )
    .await
    .expect("error updating transaction");

    Ok(())
}
