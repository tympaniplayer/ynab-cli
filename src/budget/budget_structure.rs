use chrono::{DateTime, FixedOffset};
use serde::Deserialize;

#[derive(Deserialize)]
pub struct Response {
    pub data: Data,
}
#[derive(Deserialize)]
pub struct Data {
    pub budgets: Vec<Budget>,
    pub default_budget: Option<Budget>,
}

#[derive(Deserialize)]
pub struct Budget {
    pub id: String,
    pub name: String,
    pub last_modified_on: DateTime<FixedOffset>,
    pub first_month: String,
    pub last_month: String,
    pub date_format: DateFormat,
    pub currency_format: CurrencyFormat,
    pub accounts: Vec<Account>,
}

#[derive(Deserialize)]
pub struct DateFormat {
    pub format: String,
}

#[derive(Deserialize)]
pub struct CurrencyFormat {
    pub iso_code: String,
    pub example_format: String,
    pub decimal_digits: usize,
    pub decimal_separator: String,
    pub symbol_first: bool,
    pub group_separator: String,
    pub currency_symbol: String,
    pub display_symbol: bool,
}

#[derive(Deserialize)]
pub struct Account {
    pub id: String,
    pub name: String,
    pub r#type: String,
    pub on_budget: bool,
    pub closed: bool,
    pub note: Option<String>,
    pub balance: f64,
    pub cleared_balance: f64,
    pub uncleared_balance: f64,
    pub transfer_payee_id: String,
    pub direct_import_linked: bool,
    pub direct_import_in_error: bool,
    pub deleted: bool,
}
