use serde::Deserialize;
#[derive(Deserialize)]
pub struct Response {
    pub data: Data,
}

#[derive(Deserialize)]
pub struct Data {
    pub payee: Payee,
}

#[derive(Deserialize)]
pub struct Payee {
    pub id: String,
    pub name: String,
    pub transfer_account_id: String,
    pub deleted: bool,
}
