use chrono::NaiveDate;
use serde::{Deserialize, Serialize};
#[derive(Deserialize)]
pub struct Response {
    pub data: Data,
}

#[derive(Deserialize)]
pub struct Data {
    pub transactions: Vec<Transaction>,
    pub server_knowledge: usize,
}

#[derive(Serialize)]
pub struct Request {
    pub transaction: Transaction,
}

#[derive(Deserialize, Serialize, Clone)]
pub struct Transaction {
    pub id: String,
    pub date: NaiveDate,
    pub amount: i64,
    pub memo: Option<String>,
    pub cleared: String,
    pub approved: bool,
    pub flag_color: Option<String>,
    pub account_id: String,
    pub payee_id: String,
    pub category_id: Option<String>,
    pub category_name: String,
    pub transfer_account_id: Option<String>,
    pub transfer_transaction_id: Option<String>,
    pub matched_transaction_id: Option<String>,
    pub import_id: Option<String>,
    pub deleted: bool,
    pub payee_name: String,
    pub subtransactions: Vec<Subtransaction>,
}

#[derive(Deserialize, Serialize, Clone)]
pub struct Subtransaction {
    pub id: String,
    pub transaction_id: String,
    pub amount: f64,
    pub memo: String,
    pub payee_id: String,
    pub payee_name: String,
    pub category_id: String,
    pub category_name: String,
    pub transfer_account_id: String,
    pub transfer_transaction_id: String,
    pub deleted: bool,
}
