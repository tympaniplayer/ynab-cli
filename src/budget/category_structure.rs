use serde::Deserialize;

#[derive(Deserialize)]
pub struct Response {
    pub data: Data,
}

#[derive(Deserialize)]
pub struct Data {
    pub category_groups: Vec<CategoryGroup>,
    pub server_knowledge: usize,
}

#[derive(Deserialize, Clone)]
pub struct CategoryGroup {
    pub id: String,
    pub name: String,
    pub hidden: bool,
    pub deleted: bool,
    pub categories: Vec<Category>,
}

#[derive(Deserialize, Clone)]
pub struct Category {
    pub id: String,
    pub category_group_id: String,
    pub name: String,
    pub hidden: bool,
    pub original_category_group_id: Option<String>,
    pub note: Option<String>,
    pub budgeted: i64,
    pub activity: i64,
    pub balance: i64,
    pub goal_type: Option<String>,
    pub goal_creation_month: Option<String>,
    pub goal_target: i64,
    pub goal_target_month: Option<String>,
    pub goal_percentage_complete: Option<i64>,
    pub goal_months_to_budget: Option<i32>,
    pub goal_under_funded: Option<i64>,
    pub goal_overall_funded: Option<i64>,
    pub goal_overall_left: Option<i64>,
    pub deleted: bool,
}
