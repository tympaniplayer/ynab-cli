use reqwest::Client;
use serde_json::Value;
pub mod budget;
const API: &str = "https://api.youneedabudget.com/v1/";

pub async fn list_budgets_json(
    client: &Client,
    apikey: &str,
    mut writer: impl std::io::Write,
) -> Result<(), Box<dyn std::error::Error>> {
    let response: Value = client
        .get(format!("{0}{1}", API, "budgets?include_accounts=true"))
        .bearer_auth(&apikey)
        .send()
        .await?
        .json()
        .await?;

    writeln!(writer, "{:#?}", response[0])?;

    Ok(())
}
